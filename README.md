# fractured-space-rips

An archive of assets from the game Fractured Space to keep for posterity. Refer to the Fractured Space EULA (https://store.steampowered.com/eula/310380_eula_0) for legal rights and usage of these assets.